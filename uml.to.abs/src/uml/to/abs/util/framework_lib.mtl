[comment encoding = UTF-8 /]
[module framework_lib('http://www.eclipse.org/uml2/5.0.0/UML')]
[import uml::to::abs::services::additional_services /]
[import uml::to::abs::util::util /]


[template public generateFrameworkLib(pkg : Package)]
[if (pkg.hasStereotype('vm'))]
[pkg.genHTTPRequest() /]
[pkg.genHTTPResponse() /]
[pkg.genUtility() /]
[pkg.genSQLHelper() /]
[/if]
[/template]

[template public genHTTPResponse(pkg : Package)]
[file ('abs/framework/ABSHttpResponse'+ '.abs', false)]
module ABS.Framework.Http.Response;
export *;

interface ABSHttpResponse {
    Int getStatusCode(String statusCode);
    String getDescription(String description);
    Unit setStatusCode();
    Unit setDescription();
    Unit create(Int statusCode, String description);
}

class ABSHttpResponseImpl implements ABSHttpResponse {
    Int statusCode = 0;
    String description = "";

    Int getStatusCode(String statusCode){
        return this.statusCode;
    }
    String getDescription(String description){
        return this.description;
    }
    Unit setStatusCode(){ this.statusCode = statusCode; }
    Unit setDescription(){ this.description = description; }

    Unit create(Int statusCode, String description){
        this.statusCode = statusCode;
        this.description = description;
    }
}
[/file]
[/template] 

[template public genHTTPRequest(pkg : Package)]
[file ('abs/framework/ABSHttpRequest'+ '.abs', false)]
module ABS.Framework.Http;
export *;

import Utility, UtilityImpl from ABS.Framework.Utility;

interface ABSHttpRequest {
    String getInput(String key);
    String getRequestProperty(String key);
    Bool hasKey(String key);
    Bool validateKey(Set<String> keyValid);
    Bool validateEmail(String email);
    Bool validateDomain(String domains);
    Bool validateName(String name);
    Bool validateAmount(String donation);
    Bool validateId(List<Int> ids, String id);
    Bool validatePhoneNumber(String phoneNumber);
    Bool validateAccNum(String accNum);
}

class ABSHttpRequestImpl(Map<String, String> requestInput, Map<String, String> requestProperty) implements ABSHttpRequest {
    String getInput(String key) {
        String value = fromJust(lookup(requestInput, key));
        return value;
    }

    String getRequestProperty(String key) {
        String value = fromJust(lookup(requestProperty, key));
        return value;
    }

    Bool hasKey(String key) {
        Maybe<String> maybeKey = lookup(requestInput, key);
        Bool hasKey = isJust(maybeKey);
        return hasKey;
    }

    // created by : Azhar Difa Arnanda
    Bool validateKey(Set<String> keyValid) {
        Bool isValid = False;
        Set<String> keyProvided = keys(requestInput);
        if (keyValid == keyProvided) {
            isValid =  True;
        }
        return isValid;
    }

    /*
        Method : validateEmail
        Parameter : String email
        Return value : boolean
        An email is valid when has structure:
            "username@firstDomain.secondDomain.others"
            1. username can any characters and can't a empty string
            2. @ is required after username and before firstDomain
            3. firstDomain and secondDomain must split by "."
    */

    Bool validateEmail(String email){
        Utility utility = new local UtilityImpl();
        Bool result = True;
        Bool validDom = True; 
        
        Int indexSep = utility.getIndex(email, "@");
        if (indexSep == -1) { result = False; } // no contain "@" 
        else {
            List<String> splitEmail = utility.splitString(email, "@"); 
            Int size = length(splitEmail);
            if (size != 2) { result = False; } // doesn't match sturcture
            else {
                String domains = nth(splitEmail, 1);
                validDom = this.validateDomain(domains);
            }
        }

        if (!validDom) { result = False; }
            
        return result;
    }

    /* 
        Method : validateDomain
        Parameter : String domains
        Return value : boolean
        Domains is valid when :
            1. have 2 domain, first and second
            2. second domain must have length more than or equal 2
    */
    Bool validateDomain(String domains){
        Utility utility = new local UtilityImpl();
        Bool result = True;
        String secondDomain = "";
              
        Int indexDot = utility.getIndex(domains, ".");
        if (indexDot == -1) { result = False; } // no contain "."
        else {
            List<String> splitDomains = utility.splitString(domains, ".");
            Int sizeDom = length(splitDomains);
            secondDomain = nth(splitDomains, 1);
            Int lenSD = strlen(secondDomain);
            if (lenSD < 2) {  result = False; } // second domain must have length >= 2
        }
        return result;
    }

    /* 
        Method : validateName
        Parameter : String name
        Return value : boolean
        A name is valid when only contain letters, dot, and apostrophe
    */
    Bool validateName(String name){
        Utility utility = new local UtilityImpl();
        Bool result = True;
        Int fault = 0;
        Int lenName = strlen(name);
        Int index = 0;

        while (index<lenName) {
            String char = substr(name, index, 1);
            
            // Character only from letter, dot, space, and apostrophe
            Bool condition1 = (char > "A");
            Bool condition2 = (char < "Z");
            Bool condition3 = (char > "a");
            Bool condition4 = (char < "z");
            Bool condition5 = (char == "A");
            Bool condition6 = (char == "Z");
            Bool condition7 = (char == "a");
            Bool condition8 = (char == "z"); 
            Bool condition9 = (char == "."); 
            Bool condition10 = (char == "'");
            Bool condition11 = (char == " ");
            if (!condition1 && !condition2 && !condition3 && !condition4 && !condition5 && !condition6
                && !condition7 && !condition8 && !condition9 && !condition10 &&!condition11){
                fault = fault + 1;
            }

            // Character not a number
            Int intChar = utility.stringToInteger(char);
            if (intChar != -1 && !condition9 && !condition10 && !condition11){ 
                fault = fault + 1;
            }
            index = index + 1;
        }

        if (fault > 0){
            result = False;
        }
    
        return result;
    }
    
    /* 
        Method : validateAmount
        Parameter : String amount
        A donation is valid when :
            1. Donation is positive integer
            2. Donation in range  [ '[' /] 10.000, 500.000.000 [ ']' /] 
    */
    Bool validateAmount(String amount){
        Utility utility = new local UtilityImpl();
        Bool result = True;
        Int fault = 0;

        Int amnt = utility.stringToInteger(amount);

        if (amnt <= -1){ fault = fault + 1; } 
        else {
            if (amnt < 10000) { fault = fault + 1; } 
            if (amnt > 500000000) { fault = fault + 1; } 
        }

        if (fault > 0){
            result = False;
        }

        return result;
    }

    /*
        Method : validateId
        Parameter : List<Int> ids, String id
        Return value : boolean
        A valid id if id in ids positive integer.
    */
    Bool validateId(List<Int> ids, String idStr){
        Utility utility = new local UtilityImpl();
        Bool result = False;
        Int checkId = utility.stringToInteger(idStr);
        Int size = length(ids);
        Int index = 0;

        while (index < size) {
            Int id = nth(ids, index);
            if (checkId == id) {
                result = True;
                size = 0; // to break
            }
            index = index + 1;
        }
        
        return result;
    }

     /*
        Method : validatephoneNumber
        Parameter : String phoneNumber
        Return value : boolean
        A valid phoneNumber number is when all digit is positive integer
    */
    Bool validatePhoneNumber(String phoneNumber){
        Utility utility = new local UtilityImpl();
        Bool result = True;
        Int size = strlen(phoneNumber);
        Int index = 0;
        Int fault = 0;
        
        // phoneNumber must have min 8 digit and max 15 digit
        if (size < 8 || size > 15) {
            fault = fault + 1;
        } else {
            while (index < size){
                String char = substr(phoneNumber, index, 1);
                Int parseChar = utility.parseDigit(char);
                
                // phoneNumber number must from integer
                if (parseChar == -1){
                    fault = fault + 1;
                    size = 0; // to break
                }
                index = index + 1;
            }
        }

        if (fault > 0){ result = False; }
        return result;
    }

    /*
        Method : validateAccNum
        Parameter : String accNum
        Acindex Number is valid when all of digit is positive integer
    */
    Bool validateAccNum(String accNum){
        Utility utility = new local UtilityImpl();
        Bool result = True;
        Int size = strlen(accNum);
        Int index = 0;
        Int fault = 0;
        
        while (index < size){
            String char = substr(accNum, index, 1);
            Int parseChar = utility.parseDigit(char);
            
            // acindex number must from integer
            if (parseChar == -1){
                fault = fault + 1;
                size = 0; // to break
            }
            index = index + 1;
        }
    
        if (fault > 0){
            result = False;
        }

        return result;
    }
}
[/file]
[/template]

[template public genUtility(pkg : Package)]
[file ('abs/framework/Utility'+ '.abs', false)]
module ABS.Framework.Utility;

export Utility, UtilityImpl;

interface Utility {
    Int stringToInteger(String s);
    Rat stringToRational(String s);
    Bool stringToBoolean(String s);
    List<String> splitString(String s, String separator);
    Int getIndex(String s, String ch);
    Int parseDigit(String ch);
    List<String> stringToListPair(String s);
    Pair<Int,Int> stringToPairInt(String s);
}

class UtilityImpl implements Utility {
    Int stringToInteger(String s) {
        String inputString = s;
        Int length = strlen(inputString);
        Int output = 0;

        if (length < 0) {
            throw PatternMatchFailException;
        }
        else {
            Bool negative = this.isNegativeNumber(inputString);

            if (negative) {
                inputString = substr(inputString, 1, length - 1);
            }

            length = strlen(inputString);
            Int idx = 0;
            Int tens = this.power(1, length);

            while (idx < length) {
                // Parse the first digit
                String ch = substr(inputString, 0, 1);
                Int digit = this.parseDigit(ch);

                // Multiply the digit to its "tens" values
                Int temp = digit * tens;

                // And add it to the output
                output = output + temp;

                // Move to the next digit
                idx = idx + 1;
                tens = tens / 10;

                // Reduce string to exclude parsed first digit
                Int remainingLength = strlen(inputString);
                inputString = substr(inputString, 1, remainingLength - 1);
            }

            if (negative) {
                output = output * -1;
            }
        }

        return output;
    }

    Rat stringToRational(String s) {
        String inputString = s;
        Int length = strlen(inputString);
        Rat output = 0;

        if (length < 1) {
            output = -1; // TODO: Should raise an exception
        }
        else {
            Bool negative = this.isNegativeNumber(inputString);

            if (negative) {
                inputString = substr(inputString, 1, length - 1);
            }

            length = strlen(inputString);
            Int idx = 0;
            Int tens = this.power(1, length);

            while (idx < length) {
                // Parse the first digit
                String ch = substr(inputString, 0, 1);
                Int digit = this.parseDigit(ch);

                // Multiply the digit to its "tens" values
                Int temp = digit * tens;

                // And add it to the output
                output = output + temp;

                // Move to the next digit
                idx = idx + 1;
                tens = tens / 10;

                // Reduce string to exclude parsed first digit
                Int remainingLength = strlen(inputString);
                inputString = substr(inputString, 1, remainingLength - 1);
            }

            if (negative) {
                output = output * -1;
            }
        }

        return output;
    }

    Bool stringToBoolean(String s) {
        return s == "True" || s == "true";
    }

    List<String> splitString(String s, String separator) {
        List<String> listWord = Nil;
        Int lenData = strlen(s);
        Int count = 0;
        Int last = 0;

        while(count < lenData) {
            String chr = substr(s,count,1);
            if(chr == separator) {
                Int startWord = 0;
                if (last > 0) {
                    startWord = last + 1;
                }
                Int lenWord = count - startWord;
                String word = substr(s,startWord,lenWord);
                listWord = appendright(listWord,word);
                last = count;
            }
            count = count +1;
            if (count == lenData) {
                Int startWord = 0;
                if (last > 0) {
                    startWord = last + 1;
                }
                Int lenWord = count - startWord;
                String word = substr(s,startWord,lenWord);
                listWord = appendright(listWord,word);
            }
        }
        return listWord;
    }

    Int power(Int a, Int b) {
        Int result = a;
        Int n = b;

        while (n > 1) {
            result = result * 10;
            n = n - 1;
        }

        return result;
    }

    Int parseDigit(String ch) {
        Int result = case ch {
            "0" => 0;
            "1" => 1;
            "2" => 2;
            "3" => 3;
            "4" => 4;
            "5" => 5;
            "6" => 6;
            "7" => 7;
            "8" => 8;
            "9" => 9;
            _ => -1;
        };

        if (result == -1) {
            throw PatternMatchFailException;
        }
        return result;
    }

    Bool isNegativeNumber(String s) {
        Int length = strlen(s);
        String firstDigit = substr(s, 0, 1);
        Bool result = False;

        return (firstDigit == "-");
    }

        /* 
        Method : getIndex
        Parameters : String string, String character
        Return value : boolean
        Purpose : to get index a character from a string
    */
    Int getIndex(String s, String ch){
        Int size = strlen(s);
        Int count = 0;
        Int index = -1;
        while (count < size){
            String chr = substr(s, count, 1);
            if (chr == ch) {
                index = count;
                size = 0; // to break
            }
            count = count + 1;
        }
        return index;
    }


    List<String> stringToListPair(String s) {
        List<String> result = Nil;
        String strNoWhiteSpace = this.deleteWhitespace(s);

        Int size = strlen(strNoWhiteSpace);
        
        String charFirst = substr(strNoWhiteSpace,0,1);
        String charLast = substr(strNoWhiteSpace,size-1,1);
        String fill = "";
        
        if (charFirst == " [ '[' /] " && charLast == " [ ']' /] ") {
            fill = substr(strNoWhiteSpace,1,size-2);
        }

        String pairs = fill + ")"; // to manipulate
        String pair = "";
        
        Int fillSize = strlen(fill);
        Int count = 0;

        while (count < fillSize) {
            String char = substr(pairs,count,1);
            pair = pair + char;
            if (char == ")") {
                count = count + 1; // to skip "," after ")"
                result = appendright(result, pair);
                pair = "";
            }
            count = count + 1;
        }
        return result;
    }

    Pair<Int,Int> stringToPairInt(String s){
        Pair<Int, Int> result = Pair(0,0);

        Int size = strlen(s);
        String charFirst = substr(s,0,1);
        String charLast = substr(s, size-1,1);
        
        Int firstInt = 0;
        Int secondInt = 0;
        
        if (charFirst == "(" && charLast == ")") {
            String fill = substr(s,1,size-2);
            Int count = 0;
            String new_fill = this.deleteWhitespace(fill);
        
            // Split new_fill to get 2 integer
            Int indexComma = this.getIndex(new_fill, ",");
            List<String> splitFill = Nil;
            if (indexComma > 0){
                splitFill = this.splitString(new_fill, ",");
                Int sizeSplit = length(splitFill);
                if (sizeSplit == 2) {
                    String sFirstInt = nth(splitFill,0);
                    String sSecondInt = nth(splitFill, 1);
                    firstInt = this.stringToInteger(sFirstInt);
                    secondInt = this.stringToInteger(sSecondInt);  
              }
            }
        }
        result = Pair(firstInt, secondInt);
        return result;
    }

    String deleteWhitespace(String s) {
        Int size = strlen(s);
        Int count = 0;
        String result = "";
        while (count<size){
            String ch = substr(s, count, 1);
            if (ch != " " && ch != "\n" && ch != "\t") { 
                result = result + ch;
            }
            count = count + 1;
        }
        return result;   
    }
}


[/file]
[/template]
[template public genSQLHelper(pkg : Package)]
[file ('abs/framework/SQLHelper'+ '.abs', false)]
module ABS.Framework.SQL;
export *;

import Utility, UtilityImpl from ABS.Framework.Utility;

interface SQLHelper {
    String createInStatement(String attribute, String commaSeparatedData);
}

class SQLHelperImpl implements SQLHelper {
    String createInStatement(String attribute, String commaSeparatedData) {
        Utility utility = new local UtilityImpl();
        List<String> listData = utility.splitString(commaSeparatedData, ",");
        
        Int size = length(listData);
        Int count = 0;
        String inStatement = "";

        while(count<size) {
            String condition = nth(listData, count);
            condition = attribute +" = "+ condition;
            inStatement = inStatement + condition;
            count = count +1;

            if (count < size) {
                inStatement = inStatement + " OR ";
            }
        }

        return inStatement;
    }
}
[/file]
[/template]
