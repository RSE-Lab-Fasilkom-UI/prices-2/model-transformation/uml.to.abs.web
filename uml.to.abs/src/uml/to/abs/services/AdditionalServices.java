package uml.to.abs.services;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Type;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import uml.to.abs.main.Generate_abs;;

public class AdditionalServices {
	static final String REMOVES_STEREOTYPE = "removes";
	static final String ADDS_STEREOTYPE = "adds";
	static final String MODIFIED_CLASS_STEREOTYPE = "modifiedClass";
	static final String MODIFIES_STEREOTYPE = "modifies";

	public boolean hasStereotype(Element el, String stereotypeName) {
		List<Stereotype> stereotypes = el.getAppliedStereotypes();
		for (Stereotype stereotype : stereotypes) {
			if (stereotype.getName().equals(stereotypeName)) {
				return true;
			}
		}
		return false;
	}

	public boolean attributeHasStereotype(Class cls, String stereotypeName) {
		List<Property> propList = cls.getAttributes();
		for (Property prop : propList) {
			if (hasStereotype(prop, stereotypeName)) {
				return true;
			}
		}

		return false;
	}

	public EList<Property> getNewAttributes(Class cls) {
		if (!hasStereotype(cls, MODIFIED_CLASS_STEREOTYPE)) {
			throw new IllegalArgumentException("Class must have modifiedClass stereotype");
		}
		EList<Property> clsAttributes = cls.getAttributes();
		EList<Property> removedAttributes = getAttributesWithStereotype(clsAttributes, REMOVES_STEREOTYPE);
		EList<Property> addedAttributes = getAttributesWithStereotype(clsAttributes, ADDS_STEREOTYPE);
		EList<Property> coreAttributes = new BasicEList<Property>();

		for (Association ass : cls.getAssociations()) {
			if (hasStereotype(ass, MODIFIES_STEREOTYPE)) {
				for (Element el : ass.getRelatedElements()) {
					Class targetClass = ((Class) el);
					if (!targetClass.equals(cls)) {
						coreAttributes = ((Class) el).getAttributes();
					}
				}
			}
		}
		
		EList<Property> result = setDifference(coreAttributes, removedAttributes);
		
		return result;
	}
	
	public Class getClientModifiedClass(Component cmp) {
		Package pkg = getClientPackage(cmp);
		for(Type type : pkg.getOwnedTypes()) {
			if(type.eClass().getName().equals("Class") && hasStereotype(type, MODIFIED_CLASS_STEREOTYPE)) {
				return (Class) type;
			}
		}
		return null;
	}
	
	public Package getClientPackage(Component cmp) {
		Package pkg = null;
		EList<Dependency> depList = cmp.getClientDependencies();
		for(Dependency dep : depList) {
			if(hasStereotype(dep, "when")) {
				return  (Package) dep.getSuppliers().get(0);
			}
		}
		return pkg;
	}
	
	public EList<Package> getRelatedPackage(Component cmp) {
		EList<Package> pkgList = new BasicEList<Package>();
		Package pkg = getClientPackage(cmp);
		pkgList = getRelatedPackageHelper(pkg, pkgList);
		return pkgList;
	}
	
	public EList<Package> getRelatedPackage(Package pkg) {
		EList<Package> initList = new BasicEList<Package>();
		EList<Package> pkgList = getRelatedPackageHelper(pkg, initList);
		return pkgList;
	}
	
	public EList<Package> getRelatedPackageHelper(Package pkg, EList<Package> pkgList) {
		if (hasStereotype(pkg, "delta")) {
			EList<Dependency> depList = pkg.getClientDependencies();
			for(Dependency dep : depList) {
				if(hasStereotype(dep, "after")) {
					Package aPkg = (Package) dep.getSuppliers().get(0);
					getRelatedPackageHelper(aPkg, pkgList);
				}
			}
			pkgList.add(pkg);
		}
		return pkgList;
	}
	
	private EList<Property> getAttributesWithStereotype(EList<Property> propList, String stereotypeName) {
		EList<Property> removedAttributes = new BasicEList<Property>();
		for (Property prop : propList) {
			if (hasStereotype(prop, stereotypeName)) {
				removedAttributes.add(prop);
			}
		}

		return removedAttributes;
	}
	
	private boolean containt(EList<Property> propList, Property propB) {
		for (Property propA : propList) {
			if(propA.getName().equals(propB.getName())) {
				return true;
			}
		}
		return false;
	}
	
	private EList<Property> setDifference(EList<Property> propListA, EList<Property> propListB) {
		EList<Property> result = new BasicEList<Property>();
		for (Property propA : propListA) {
			if (!containt(propListB, propA)) {
				result.add(propA);
			}
		}
		return result;
	}
	
	public boolean deleteLastCurlyBracket(String filepath) {
		Scanner sc = null;
		FileWriter fw = null;
		try {
			String newPath = Generate_abs.getGeneratedPath() + "/" + filepath;
//			System.out.println(newPath);
			sc = new Scanner(new File(newPath));
			sc.useDelimiter("\\Z");
			String content = sc.next();
			String withoutLastCharacter = content.substring(0, content.length() - 1);
			
			fw = new FileWriter(newPath);
			fw.write(withoutLastCharacter);
//			System.out.println("delete success");
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				sc.close();
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
