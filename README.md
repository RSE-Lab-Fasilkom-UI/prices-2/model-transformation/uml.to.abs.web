# UML to ABS Web Transformation Tools
The UML-DOP Profile is designed to support UML diagram transformation for ABS.
Using the profile, transformation from UML class diagram to ABS is straightforward. 
The generated ABS source follows the structure of ABS microservice framework.
The transformation rules is implemented in Eclipse Acceleo Model to Text Transformation. 

## Getting started
1. Download Eclipse Modeling Tools (2020-12)
2. Install Papyrus Eclipse Plugin (The UML diagram editor)

## Instalation
1. Clone this project to your computer
2. Open Eclipse and import this tool as Eclipse project


## Running Example - AMANAH project
1. Clone UML-DOP diagram for AMANAH from [this repository](https://gitlab.com/RSE-Lab-Fasilkom-UI/prices-2/model-transformation/aisco-uml-dop/-/tree/uml-abs-ms?ref_type=heads)
2. Import as Eclipse project
3. Right click on the file `aisco-uml-abs.uml`
4. The generated source code will be available in directory `src-gen`

